from timeit import timeit

import torch
from torch import nn, Tensor, optim
from torch.utils.data import DataLoader
from torchvision.datasets.mnist import MNIST
from torchvision.transforms import transforms

ROOT: str = '.'
MODEL_PATH: str = 'model.dict'

IMAGE_SIZE: int = 28
NUMBER_OF_CLASSES: int = 10
IMAGE_CHANNELS_COUNT: int = 1


def load_mnist(train: bool = True) -> MNIST:
    return MNIST(root=ROOT, download=True, train=train, transform=transforms.ToTensor())


def out_size(
        h_in: int,
        w_in: int,
        padding: tuple[int, ...],
        kernel_size: tuple[int, ...],
        stride: tuple[int, ...]
) -> tuple[int, int]:
    return (
        (h_in + 2 * padding[0] - kernel_size[0]) // stride[0] + 1,
        (w_in + 2 * padding[1] - kernel_size[1]) // stride[1] + 1,
    )


class Model(nn.Module):

    def __init__(self) -> None:
        super().__init__()

        layers: list[nn.Module] = []

        conv1: nn.Conv2d = nn.Conv2d(
            in_channels=IMAGE_CHANNELS_COUNT,
            out_channels=7,
            kernel_size=(5, 5),
            stride=(1, 1)
        )
        layers += [conv1, nn.ReLU()]

        conv2: nn.Conv2d = nn.Conv2d(
            in_channels=conv1.out_channels,
            out_channels=6,
            kernel_size=(5, 5),
            stride=(1, 1)
        )
        layers += [conv2, nn.ReLU()]

        maxpool: nn.MaxPool2d = nn.MaxPool2d(
            kernel_size=(2, 2),
            stride=(2, 2)
        )
        layers += [maxpool, nn.Flatten()]

        conv1_out_size: tuple[int, int] = \
            out_size(IMAGE_SIZE, IMAGE_SIZE, conv1.padding, conv1.kernel_size, conv1.stride)

        conv2_out_size: tuple[int, int] = \
            out_size(conv1_out_size[0], conv1_out_size[1], conv2.padding, conv2.kernel_size, conv2.stride)

        maxpool_out_size: tuple[int, int] = \
            out_size(conv2_out_size[0], conv2_out_size[1], (0, 0), maxpool.kernel_size, maxpool.stride)

        flattened_size: int = maxpool_out_size[0] * maxpool_out_size[1] * conv2.out_channels

        fc1: nn.Linear = nn.Linear(
            in_features=flattened_size,
            out_features=150
        )
        layers += [fc1, nn.ReLU()]

        fc2: nn.Linear = nn.Linear(
            in_features=fc1.out_features,
            out_features=NUMBER_OF_CLASSES
        )
        layers += [fc2, nn.LogSoftmax(dim=1)]

        self.layers: nn.Module = nn.Sequential(*layers)

    def forward(self, x: Tensor) -> Tensor:
        return self.layers(x)


def train_model(
        model: Model,
        data_loader: DataLoader[Tensor],
        print_period: int = 25
) -> None:
    loss_fn: nn.NLLLoss = nn.NLLLoss()
    optimizer: optim.Optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.95, nesterov=True)

    total_batches: int = len(data_loader)
    max_printed_len: int = 0

    for batch, (data, label) in enumerate(data_loader, start=1):
        data: Tensor
        label: Tensor

        predicted: Tensor = model(data)
        loss: Tensor = loss_fn(predicted, label)

        if batch % print_period == 0:
            msg: str = f'\rbatch {batch}/{total_batches} loss = {loss.item():.4f}'
            max_printed_len = max(max_printed_len, len(msg))
            print(msg, end='')

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    print('\r' + ' ' * max_printed_len, end='\r')


def test_model(
        model: Model,
        data_loader: DataLoader[Tensor],
        print_period: int = 250
) -> None:
    correct: int = 0
    total: int = len(data_loader)

    class_counts: dict[int, int] = {}

    with torch.no_grad():
        for input_no, (data, label) in enumerate(data_loader, start=1):
            data: Tensor
            label: Tensor

            class_counts[label.item()] = 1 + class_counts.get(label.item(), 0)

            predicted: Tensor = torch.argmax(model(data), dim=1)
            if label == predicted:
                correct += 1

            if input_no % print_period == 0 or input_no == total:
                print(f'\rtesting {input_no}/{total}', end='')

    print(f'\rprecision = {correct / total * 100:.2f}%' + ' ' * 10)
    print(f'class samples distribution:')
    for label, samples_count in sorted(class_counts.items(), key=lambda it: it[0]):
        print(f'{label}: {samples_count / total * 100:.2f}%')


def main() -> None:
    train_set: MNIST = load_mnist()
    train_set: DataLoader = DataLoader(train_set, batch_size=20, shuffle=True)

    test_set: MNIST = load_mnist(train=False)
    test_set: DataLoader = DataLoader(test_set)

    model = Model()
    try:
        model.load_state_dict(torch.load(MODEL_PATH))
        model.eval()
    except IOError:
        time = timeit(lambda: train_model(model, train_set), number=1)
        print(f'train time = {time:.2f} s')
        torch.save(model.state_dict(), MODEL_PATH)

    test_model(model, test_set)


if __name__ == '__main__':
    main()
